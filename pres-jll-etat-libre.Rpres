L'état de l'informatique libre au Québec
========================================================
author: François Pelletier
date: 17 septembre 2016
autosize: false

Informatique libre ?
========================================================

- Pas seulement des logiciels libres ?
- Le mouvement du logiciel libre a mené à l'informatique libre
- Particulièrement avec l'arrivée d'Internet

FACIL ?
========================================================

![](logo_Facil.svg)

- FACIL, pour l'appropriation collective de l'informatique libre, se donne comme mandat de promouvoir directement ou indirectement, l'adoption, l'usage et la démocratisation de l'informatique libre et des standards ouverts sous-jacents au sein de la population du Québec et de ses diverses institutions publiques, ainsi qu'auprès des entreprises et des organismes.

FACIL ?
========================================================

- L'informatique n'a pas nécessairement toujours été abordable.
- Plusieurs technologies qui évoluent très rapidement
- Nouveaux enjeux: confidentialité, DRM, surveillance de masse, communs numériques
- Lien direct avec le logiciel libre

Aujourd'hui
========================================================

Vous faire découvrir la richesse de l'informatique libre au Québec

Contenu
========================================================
- Les acteurs du libre
- Les licences de logiciel libre québécoises
- Les évènements
- Les projets en cours
- Web et réseaux sociaux
- Les publications
- Revue de l'actualité concernant l'informatique libre

Les acteurs du libre
========================================================
- Les associations
- Les communautés d'utilisateurs
- Les ateliers
- Les entreprises
- Les organismes gouvernementaux
- Le système d'éducation

Les associations
========================================================

- Promotion d'une cause
- Agissent en tant que représentants d'individus et d'entreprises dans la société civile.

- Plus de 20 associations au Québec 
- Leur mission principale ou celle d'un de leurs groupes d'action concerne un enjeu de l'informatique libre.

Les communautés d'utilisateurs
========================================================

- Près de 40 communautés d'utilisateurs 
- Concentrées autour du logiciel libre de façon générale 
- Autour d'un outil en particulier:
  - langages de programmation
  - plateformes de publication web

Les ateliers
========================================================

- Démocratisation de la fabrication d'objets: 
  - Éléments technologiques
  - Communauté d'entraide
  
- Promotion d'une informatique libre:
  - Intégration de modules embarqués (ex.: Raspberry Pi)
  - Partage de plans de conception et d'impression 3D
  - Utilisation de matériel libre (ex.: Arduino)

Les ateliers
========================================================

Selon le site web [FABLABS Québec](http://wiki.fablabs-quebec.org/index.php?title=Les_Fab_Labs_au_Qu%C3%A9bec), on dénombre actuellement 37 ateliers de fabrication numérique, aussi appelés FAB labs, makerspace, hackerspace ...

![](carte-fablabs.png)

Les entreprises
========================================================

Plus de 30 entreprises se spécialisent dans la production ou le soutien technique de logiciels libres.
Certaines entreprises offrent aussi l'hébergement en mode logiciel à la demande.

Les organismes gouvernementaux
========================================================

Le Secrétariat du Conseil du Trésor est notamment responsable du portail de données ouvertes [Données Québec](http://www.donneesquebec.ca). Ce nouveau portail publié en 2016 (utilisant des logiciels libres !) rassemble des données ouvertes provinciales et municipales.

Le regroupement IGO développe et maintient le logiciel Infrastructure Géomatique Ouverte, qui permet la visualisation de données géographiques.

Le gouvernement fédéral maintient aussi l'imposant portail de données ouvertes [gouvernement ouvert](ouvert.canada.ca/fr/donnees-ouvertes)

Les organismes gouvernementaux
========================================================

En 2015, le Centre de services partagés du Québec a publié deux licences de logiciels libres.

Le centre d'expertise en logiciel libre (CELL), malgré sa cure minceur imposée par le gouvernement actuel, subsiste et tente d'influencer l'adoption de solutions libres, malgré le poids imposant des grandes firmes. Certaines initiatives ministérielles ont actuellement lieu, mais sont difficiles à recenser. Votre contribution est la bienvenue !

Le système d'éducation
========================================================

- Au collégial:
  - Certains établissements d'enseignement offrent des cours de formation continue basés sur des logiciels libres. Le Collège de Maisonneuve est un pionnier en cette matière.
  - Plusieurs associations représentent les collèges dans le domaine du logiciel libre et du matériel éducatif libre.

- À l'université:
  - Plusieurs centres de recherche développent des logiciels libres et plusieurs professeurs publient du matériel pédagogique libre.

Les développeurs et contributeurs
========================================================

Selon Robin Millette, principal contributeur au projet [RoLLodeQc](http://www.rollodeqc.com/), plus de 5000 Québécois ont été actifs sur le site de développeurs [GitHub](https://www.github.com), dont 450 sur une base hebdomadaire.

Les langages de programmation les plus populaires sont:
- Java (16%)
- JavaScript (15%)
- C++ (13%)
- C (11%)
- Python (8%)

Les licences de logiciel libre québécoises
========================================================

Trois licences de logiciel libre québécoises ont été publiées en 2015 et reconnues en 2016 par l'Open Source Initiative [http://opensource.org/].

- [Licence Libre du Québec – Permissive (LiLiQ-P) v1.1](https://www.forge.gouv.qc.ca/participez/licence-logicielle/licence-libre-du-quebec-liliq-en-francais/licence-libre-du-quebec-permissive-liliq-p-v1-1/)
- [Licence Libre du Québec – Réciprocité (LiLiQ-R) v1.1](https://www.forge.gouv.qc.ca/licence-libre-du-quebec-reciprocite-liliq-r-v1-1/)
- [Licence Libre du Québec – Réciprocité forte (LiLiQ-R+) v1.1](https://www.forge.gouv.qc.ca/licence-libre-du-quebec-reciprocite-forte-liliq-r-v1-1/)

Les évènements
========================================================

Différents types d'évènements portant sur l'informatique libre ont eu lieu durant la dernière année:

- Évènements annuels récurrents
- Évènements mensuels récurrents
- Évènements non récurrents au Québec

Évènements annuels récurrents
========================================================

- Journée internationale de la protection des données
personnelles
- Journée internationale des données ouvertes
- Journée internationale du logiciel libre
- Journée internationale contre les DRM
- Semaine québécoise de l’informatique libre
- Salon du logiciel libre du Québec (S2LQ)
- (suite...)

Évènements annuels récurrents
========================================================
- ConFoo
- DrupalCamp Montréal
- WordCamp Montréal
- Colloque libre de l’Adte
- Mois international de la contribution francophone
- Semaine du libre-accès

Évènements mensuels récurrents
========================================================

- La plupart des groupes d'utilisateurs se rencontrent mensuellement ou selon une autre fréquence.
- L'[Agenda du Libre](http://agendadulibre.qc.ca/) et le site [Meetup](http://www.meetup.com) constituent un bon moyen de retrouver ces activités.

Évènements non récurrents au Québec
========================================================

Ces évènements d’envergure nationale ou internationale ont
été de passage au Québec durant la dernière année.

- 25th World Wide Web Conference
- W4A : Web For All Conference
- #OKFestMTL
- Forum Social Mondial
- Colloque Ouvrir la science pour mieux la partager, du
Nord au Sud de la Francophonie
- Éco2Fest

Évènement presque récurrent au Québec ?
========================================================

Nous avons aussi eu l'occasion d'accueillir trois conférences de Richard Stallman au Québec en 2016 !

Les projets en cours
========================================================

Plus de 35 logiciels libres sont en développement actif principalement par des contributeurs ou des gestionnaires de projet québécois.

- ActiveRecord-JSONValidator
- Boı̂te à outils de l’expérience Web
- Capistrano S3
- Cecilia 5
- Constellio
- Course activity planner

(suite ...)

Les projets en cours
========================================================
- Daala
- dupeGuru
- Gaffe
- Gambit
- Her
- Infrastructure Géomatique Ouverte
- LTTng
- LXD
- MapServer

(suite ...)

Les projets en cours
========================================================
- moneyGuru
- Nit
- Opus
- Orchid
- PdfMasher
- Pressbooks
- pump.io
- Pyo

(suite ...)

Les projets en cours
========================================================
- Represent
- Ring
- Scenic
- Soundgrain
- Spacemacs
- STK (The Synthesis ToolKit in C++)
- Teamocil
- Tig

(suite ...)

Les projets en cours
========================================================
- Tiki Wiki CMS Groupware
- Torride
- Trace Compass
- Vanilla
- Vega
- Wifidog

Web et réseaux sociaux
========================================================

- BlogueLinux.ca
- Les Chiens de garde
- QuebecOS
- Observatoire francophone des TI
- Un plan numérique pour le Québec
- Wiki de FACIL

Les publications
========================================================

Trois publications importantes de FACIL:
- [L’informatique libre pour une véritable participation de toutes et de tous à la culture](https://facil.qc.ca/files/memoire-politique-culturelle-du-quebec-2016.pdf)
- [Donner la priorité au logiciel libre](https://facil.qc.ca/files/facil-recommandations-strategie-gouv-ti-oct-2015.pdf)
- [L'informatique libre pour une vraie transparence](https://facil.qc.ca/files/memoire-de-facil-orientations-gouv-transparence-aout-2015.pdf) que nous avons présenté en commission parlementaire.


Revue de l'actualité concernant l'informatique libre
========================================================

Quels ont été les sujets couverts dans les médias de masse depuis un an ?

- SQIL 2015
- Droit d’auteur
- Gouvernement ouvert
- Logiciel libre dans l’administration publique
- Politiques publiques du numérique
- Protection de la vie privée

SQIL 2015
========================================================

- La présentation du film Deep Web
- Entrevue de Pierre-Yves Gosset de Framasoft

Droit d'auteur
========================================================

Logiciels = oeuvres littéraires dans le droit canadien

- Domaine public
- L'empreinte de l'Université Laval
- Partenariat Trans-Pacifique

Gouvernement ouvert
========================================================

- Ville intelligente
- Logiciels d'impôts et paradis fiscaux
- Portails de données ouvertes
- Démocratie ouverte et participative

Logiciel libre dans l'administration publique
========================================================

```
¯\_(シ)_/¯

(╯'□')╯︵ ┻━┻

ಠ_ಠ
```

Politiques publiques du numérique
========================================================

- Uber
- La stratégie numérique du Québec
- Les Étonnés
- Drones
- Plan d'action numérique
- Netflix
- Richard Stallman

Protection de la vie privée
========================================================

- 15 ans après le 11 septembre
- Loto-Québec et neutralité du net
- Ashley Madison
- Police et mot de passe
- Pokemon Go
- Phénix (SCRS)
- iPhone
- Panama Papers
- Droit à l'oubli (Google, Facebook)
- Bracelets santé et télématique